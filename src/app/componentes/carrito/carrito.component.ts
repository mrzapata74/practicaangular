import { Component, OnInit } from '@angular/core';
import { ServiciogralService } from 'src/app/servicios/serviciogral.service';
// import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

public carrito: any[] = [];


  constructor(private servicio:ServiciogralService) {
    
    this.carrito = this.servicio.carritoLista;
    console.log("Lista en el carrito constructor "+ JSON.stringify(this.carrito));

   }

  ngOnInit() {
    console.log("Lista en el carrito ngOnInit "+ JSON.stringify(this.carrito));
  }




}

