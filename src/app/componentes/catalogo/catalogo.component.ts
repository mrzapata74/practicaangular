import { Component, OnInit } from '@angular/core';
import { ServiciogralService } from '../../servicios/serviciogral.service';
import { CarritoComponent } from '../../componentes/carrito/carrito.component';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

  listaProductos: any;

  constructor(private servicio: ServiciogralService, private carro:CarritoComponent) { }

  ngOnInit() {
    this.getDatosProd();

  }

  //Tomando Datos del servicio
/*   getDatosProd(){
    return this.servicio.getDatos().subscribe((datos)=> {this.listaProductos = datos;
      console.log('Datos Totales de Firebase '+JSON.stringify(this.listaProductos))
    })
  } */
  getDatosProd(){
    return this.servicio.getDatos().subscribe((datos)=> this.listaProductos = datos)
  }
  
  /* Funcion que recibe los datos enviados desde el card producto */
  anadir(id:string, nombP: string, price: number, exist: number, cantSelec: number){
    let idProd: string = id;
    let producto: string = nombP;
    let precio: number = price;
    let existencia: number = exist;
    let cantidad: number = cantSelec;
    let subtotal: number = precio * cantidad;

    /* Creando variable objeto para enviar los datos al servicio */
    let datosAlServicio: any;

    datosAlServicio = {idProd, producto, precio, existencia, cantidad, subtotal};
    console.log("Array del catalogo TS "+ JSON.stringify(datosAlServicio) );

this.servicio.datosCardProductos(datosAlServicio);
  }
 
}
