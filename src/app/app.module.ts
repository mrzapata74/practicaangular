import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ServiciogralService } from './servicios/serviciogral.service';
import { CarritoservicioService } from './servicios/carritoservicio.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
/* Importando Pipe */
import { NgPipesModule } from 'ngx-pipes';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatalogoComponent } from './componentes/catalogo/catalogo.component';
import { CarritoComponent } from './componentes/carrito/carrito.component';

@NgModule({
  declarations: [
    AppComponent,
    CatalogoComponent,
    CarritoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    NgPipesModule,
    HttpClientModule

  ],
  providers: [ServiciogralService, CarritoservicioService, CarritoComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
