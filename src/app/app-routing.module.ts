import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Importando Componentes */
import {CatalogoComponent } from './componentes/catalogo/catalogo.component';
import {CarritoComponent } from './componentes/carrito/carrito.component';

const routes: Routes = [
  {path: 'catalogo', component: CatalogoComponent},
  {path: 'carrito', component: CarritoComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
