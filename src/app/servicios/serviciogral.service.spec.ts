import { TestBed } from '@angular/core/testing';

import { ServiciogralService } from './serviciogral.service';

describe('ServiciogralService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiciogralService = TestBed.get(ServiciogralService);
    expect(service).toBeTruthy();
  });
});
